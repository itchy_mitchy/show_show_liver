﻿using UnityEngine; 
using System.Collections;



public class ScalpelPlayerOne : MonoBehaviour {
	public static float currentScalpel1PathPercent = 0.0f;

	public static bool scalpel1Leave = false;
    

	void Start() {

        //iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("scalpel_1_path"),"time", 5, "easetype", iTween.EaseType.easeInOutSine));

    } 

	void Update() {


		if (Input.GetKeyDown ("a")) {
            Debug.Log("Entered getkeydown on scalpal");
            iTween.PutOnPath (gameObject, iTweenPath.GetPath ("scalpel_1_path"), currentScalpel1PathPercent += 0.01f);
		}
        
        if(ControlFlowManager.Singleton)
        {
            float incrementVal = ControlFlowManager.Singleton.LeftSideAvgCPSThisFrame * ControlFlowManager.Singleton.InputFactor;
            Debug.Log("Moving with increment val = " + incrementVal);
            iTween.PutOnPath(gameObject, iTweenPath.GetPath("scalpel_1_path"), currentScalpel1PathPercent += incrementVal);
        }

		if (currentScalpel1PathPercent >= 1.00f && scalpel1Leave == false)
        {
            Debug.Log("Entered ScalpelOneLeave on scalpal");
            ScalpelOneLeave ();
			scalpel1Leave = true;
            Debug.Log("Player 1 win?");
        } 

		if (JarOne.scalpel1KillCount <= 0.0f) {
            Debug.Log("Entered scalpel1KillCount on scalpal");
			Destroy (gameObject);
		}
	} 

	void ScalpelOneLeave() {
		iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("scalpel_1_leave"),"time", 3, "easetype", iTween.EaseType.easeInOutSine));
	}


	public void AdvanceOnPath()
	{
		iTween.PutOnPath (gameObject, iTweenPath.GetPath ("scalpel_1_path"), currentScalpel1PathPercent += 0.001f);
	}
}

